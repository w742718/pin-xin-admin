# web
 
目前就一个主干

# 安装、运行、打包
```
安装依赖项
yarn install  或  npm install 已经镜像 cnpm install

本地调试
yarn serve  或 npm run dev


打包
yarn build 或 npm run build 
```

### 自定义配置
See [Configuration Reference](https://cli.vuejs.org/config/).

# 结构说明
```
├─assets        静态资源
├─components    常用组件
├─config        全局配置文件
├─lib           公用库
│  ├─directives 指令 directive
│  ├─filters    过滤器 filter
│  ├─plugins    插件 install()
│  ├─styles     全局样式
│  └─utils      工具库
├─locale        国际化
├─mock          模拟数据(仅供本地测试,打包需注释掉index.js中的引入)
│  └─modules      模拟数据子模块
├─router        路由配置
│  └─modules      路由配置子模块
├─api          接口API定义
│  ├─home        API接口子模块
│  └─system      API接口子模块
├─store         vuex存储
│  └─modules      store子模块
└─views         路由页面(新加页面都是在这里配置添加..)
    ├─components      公用模块(401、403、404、405)
    └─home        首页
  
```

# 模式和环境变量
#### 模式
```
yarn build
Vue CLI 项目有三个模式：
1. development 用于 vue-cli-service serve
2. production 用于 vue-cli-service build
3. test 用于 vue-cli-service test:unit
```

可以通过传递 --mode 选项参数为命令行覆写默认的模式。
```
"dev-build": "vue-cli-service build --mode development",
```

### 环境变量
自定义全局变量,定义在.env[xx]文件下.使用规则如下。
```
.env                # 在所有的环境中被载入
.env.local          # 在所有的环境中被载入，但会被 git 忽略
.env.[mode]         # 只在指定的模式中被载入
.env.[mode].local   # 只在指定的模式中被载入，但会被 git 忽略
```
> 注: 变量名只有以 VUE_APP_XXX 开头才会被 webpack.DefinePlugin 静态嵌入到客户端侧的包中。
>
> 用法: process.env.VUE_APP_XXX
