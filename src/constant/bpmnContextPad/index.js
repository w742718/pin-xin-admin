/*
 * @Author: liaoxing
 * @Date: 2021-10-28 15:37:14
 * @LastEditors: liaoxing
 * @LastEditTime: 2021-10-28 15:38:19
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\constant\bpmnContextPad\index.js
 */
import CustomContextPadProvider from "./contextPadProvider";

export default {
  __init__: ["contextPadProvider"],
  contextPadProvider: ["type", CustomContextPadProvider]
};