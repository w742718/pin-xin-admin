/*
 * @Author: liaoxing
 * @Date: 2021-10-18 16:41:13
 * @LastEditors: liaoxing
 * @LastEditTime: 2021-10-27 14:22:10
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\constant\xmlStr.js
 */
// let temp = `<?xml version="1.0" encoding="UTF-8"?>
// <bpmn:definitions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" id="Definitions_1" targetNamespace="http://bpmn.io/schema/bpmn">
//   <bpmn:process id="Process_1" isExecutable="false">
//   </bpmn:process>
//   <bpmndi:BPMNDiagram id="BPMNDiagram_1">
//     <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_1">
//     </bpmndi:BPMNPlane>
//   </bpmndi:BPMNDiagram>
// </bpmn:definitions>`

let temp = (id, name) => `<?xml version="1.0" encoding="UTF-8"?>
  <definitions xmlns="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:flowable="http://flowable.org/bpmn" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:omgdc="http://www.omg.org/spec/DD/20100524/DC" xmlns:omgdi="http://www.omg.org/spec/DD/20100524/DI" xmlns:xsd="http://www.w3.org/2001/XMLSchema" targetNamespace="http://flowable.org/test" exporter="Flowable Open Source Modeler">
  <process id="${id}" name="${name}" isExecutable="true">
    <startEvent id="startEvent1" />
  </process>
  <bpmndi:BPMNDiagram id="BPMNDiagram_gt132">
    <bpmndi:BPMNPlane id="BPMNPlane_gt132" bpmnElement="gt132">
      <bpmndi:BPMNShape id="BPMNShape_startEvent1" bpmnElement="startEvent1">
        <omgdc:Bounds x="100" y="163" width="30" height="30" />
      </bpmndi:BPMNShape>
    </bpmndi:BPMNPlane>
  </bpmndi:BPMNDiagram>
</definitions>`

// let temp = `<?xml version="1.0" encoding="UTF-8"?>
// <bpmn:definitions xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" xmlns:camunda="http://camunda.org/schema/1.0/bpmn" xmlns:di="http://www.omg.org/spec/DD/20100524/DI" id="Definitions_1" targetNamespace="http://bpmn.io/schema/bpmn">
//   <bpmn:process id="Process_1" isExecutable="false">
//     <bpmn:startEvent id="Event_0c1jqyi" name="卡斯基">
//       <bpmn:extensionElements>
//         <camunda:inputOutput>
//           <camunda:inputParameter name="id">Event_0c1jqyi</camunda:inputParameter>
//           <camunda:inputParameter name="createTime">2019</camunda:inputParameter>
//           <camunda:inputParameter name="list">
//             <camunda:list>
//               <camunda:value>{"name":"测试999","data":"数值999"}</camunda:value>
//               <camunda:value>{"name":"测试888","data":"数值888"}</camunda:value>
//             </camunda:list>
//           </camunda:inputParameter>
//           <camunda:inputParameter name="obj">
//             <camunda:map>
//               <camunda:entry key="name">nice</camunda:entry>
//               <camunda:entry key="data">还不错</camunda:entry>
//             </camunda:map>
//           </camunda:inputParameter>
//         </camunda:inputOutput>
//       </bpmn:extensionElements>
//       <bpmn:outgoing>Flow_1lma0cj</bpmn:outgoing>
//     </bpmn:startEvent>
//     <bpmn:task id="Activity_004bvkn" name="流程1">
//       <bpmn:extensionElements>
//         <camunda:inputOutput>
//           <camunda:inputParameter name="id">Activity_004bvkn</camunda:inputParameter>
//           <camunda:inputParameter name="createTime">6060</camunda:inputParameter>
//           <camunda:inputParameter name="list">
//             <camunda:list>
//               <camunda:value>{"name":"测试1","data":"数值1"}</camunda:value>
//             </camunda:list>
//           </camunda:inputParameter>
//           <camunda:inputParameter name="obj">
//             <camunda:map>
//               <camunda:entry key="name">士大夫</camunda:entry>
//               <camunda:entry key="data">舍得</camunda:entry>
//             </camunda:map>
//           </camunda:inputParameter>
//         </camunda:inputOutput>
//       </bpmn:extensionElements>
//       <bpmn:incoming>Flow_1lma0cj</bpmn:incoming>
//     </bpmn:task>
//     <bpmn:sequenceFlow id="Flow_1lma0cj" sourceRef="Event_0c1jqyi" targetRef="Activity_004bvkn" />
//   </bpmn:process>
//   <bpmndi:BPMNDiagram id="BPMNDiagram_1">
//     <bpmndi:BPMNPlane id="BPMNPlane_1" bpmnElement="Process_1">
//       <bpmndi:BPMNEdge id="Flow_1lma0cj_di" bpmnElement="Flow_1lma0cj">
//         <di:waypoint x="498" y="310" />
//         <di:waypoint x="550" y="310" />
//       </bpmndi:BPMNEdge>
//       <bpmndi:BPMNShape id="Event_0c1jqyi_di" bpmnElement="Event_0c1jqyi">
//         <dc:Bounds x="462" y="292" width="36" height="36" />
//         <bpmndi:BPMNLabel>
//           <dc:Bounds x="463" y="335" width="34" height="14" />
//         </bpmndi:BPMNLabel>
//       </bpmndi:BPMNShape>
//       <bpmndi:BPMNShape id="Activity_004bvkn_di" bpmnElement="Activity_004bvkn">
//         <dc:Bounds x="550" y="270" width="100" height="80" />
//       </bpmndi:BPMNShape>
//     </bpmndi:BPMNPlane>
//   </bpmndi:BPMNDiagram>
// </bpmn:definitions>`

export default temp