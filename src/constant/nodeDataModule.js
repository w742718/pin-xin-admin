/*
 * @Author: liaoxing
 * @Date: 2021-10-19 13:53:24
 * @LastEditors: liaoxing
 * @LastEditTime: 2021-11-04 09:51:16
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\constant\nodeDataModule.js
 */
export default () => {
  return {
    id: '',
    name: '',
    assignee: '',
    initiator: '',
    condition: ''
  }
}