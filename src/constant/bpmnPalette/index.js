/*
 * @Author: liaoxing
 * @Date: 2021-10-28 15:07:12
 * @LastEditors: liaoxing
 * @LastEditTime: 2021-10-28 15:17:44
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\constant\bpmnPalette\index.js
 */
import CustomPalette from "./CustomPalette";

export default {
  __init__: ["paletteProvider"],
  paletteProvider: ["type", CustomPalette]
};