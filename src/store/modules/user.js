import { login, logout,  refreshToken } from '@/api/login'
import { getToken, setToken, setExpiresIn, removeToken } from '@/utils/auth'
import { getLoginToken, getInfos } from '@/api/autoToken'
import sessionStorage from '@/utils/sessionStorage'
import loginRequest from '@/api/pxLogin'
import cookies from '@/utils/cookies'
const user = {
  state: {
    token: getToken(),
    name: '',
    avatar: '',
    roles: [],
    permissions: []
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_EXPIRES_IN: (state, time) => {
      state.expires_in = time
    },
    SET_NAME: (state, name) => {
      state.name = name
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_PERMISSIONS: (state, permissions) => {
      state.permissions = permissions
    }
  },

  actions: {
    // 登录
    Login({ commit }, userInfo) {
    //   const username = userInfo.username.trim()
    //   const password = userInfo.password
    //   const code = userInfo.code
    //   const uuid = userInfo.uuid
    //   return new Promise((resolve, reject) => {
    //     login(username, password, code, uuid).then(res => {
    //       let data = res.data
    //       setToken(data.access_token)
    //       commit('SET_TOKEN', data.access_token)
    //       setExpiresIn(data.expires_in)
    //       commit('SET_EXPIRES_IN', data.expires_in)
    //       localStorage.setItem('job_number',data.job_number)
    //       resolve()
    //     }).catch(error => {
    //       reject(error)
    //     })
    //   })
      // let params = Object.assign({
      //   client_id: 'pinxin',
      //   client_secret: 'pinxin@666',
      //   username: userInfo.username.trim(),
      //   password:  userInfo.password,
      //   grant_type: 'password',
      //   uuid: userInfo.uuid
      // })
      return new Promise((resolve, reject) => {
        if (userInfo) {
          let params = {
            account: userInfo.username,
            password: userInfo.password
          }
          console.log(params)
          loginRequest.pxLogin(params).then(res => {
            if (res.code == 200) {
              console.log("角色：")
              console.log(res.data)
              cookies.setCookie('userInfo', res.data)
              setToken('res.access_token')
              commit('SET_TOKEN', 'res.access_token')
              setExpiresIn('res.expires_in')
              commit('SET_EXPIRES_IN', 'res.expires_in')
              resolve()
            } else {
              resolve(res)
            }
          }).catch(error => {
            reject(error)
          })
        } else {
          setToken('res.access_token')
          commit('SET_TOKEN', 'res.access_token')
          setExpiresIn('res.expires_in')
          commit('SET_EXPIRES_IN', 'res.expires_in')
          resolve()
        }
        
        // getLoginToken(params).then((res) => {
        //   console.log(res)
        // setToken('res.access_token')
        // commit('SET_TOKEN', 'res.access_token')
        // setExpiresIn('res.expires_in')
        // commit('SET_EXPIRES_IN', 'res.expires_in')
        //     // localStorage.setItem('job_number',data.job_number)
        //   resolve()
        // }).catch(error => {
        //   reject(error)
        // })
        // resolve()
      })
    },

    // 获取用户信息
    GetInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        
        // getInfos().then(res => {
        //   console.log(res)
        // })
        //   let data = res.data
        //   const user = data.user
        //   const avatar = !user.avatar ? require("@/assets/images/profile.jpg") : user.avatar;
        //   if (data.roles && data.roles.length > 0) { // 验证返回的roles是否是一个非空数组
            commit('SET_ROLES', "data.roles")
            commit('SET_PERMISSIONS', "data.permissions")
        //   } else {
          // commit('SET_ROLES', ['ROLE_DEFAULT'])       
          // }
          // commit('SET_NAME', user.nickName)
          // commit('SET_AVATAR', avatar)
          // resolve(data)
          resolve()
        // }).catch(error => {
        //   reject(error)
        // })
      })
    },

    // 刷新token
    RefreshToken({commit, state}) {
      return new Promise((resolve, reject) => {
        refreshToken(state.token).then(res => {
          setExpiresIn(res.data)
          commit('SET_EXPIRES_IN', res.data)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },
    
    // 退出系统
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(() => {
          commit('SET_TOKEN', '')
          commit('SET_ROLES', [])
          commit('SET_PERMISSIONS', [])
          removeToken()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 前端 登出
    FedLogOut({ commit }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '')
        // sessionStorage.clear()
        removeToken()
        resolve()
      })
    }
  }
}

export default user
