/*
 * @Author: liaoxing
 * @Date: 2022-02-25 17:50:31
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-03-01 15:12:20
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\api\report\newApi.js
 */
// import serve from '@/utils/pinxin/liquanchun'
// import serve2 from '@/utils/report'
import serve from '@/utils/report'
// const uri = process.env.NODE_ENV === 'development' ? '/xiaoxinyan' : '/report/api/v1'

export default {
  /* 查询报表 */
  queryReport: (params) => {
    let para = getparams(params)
    return serve.get('/rs/api/v1/report?' + para)
  },
  /* 查询报表详情 */
  queryReportDetails: (params) => {
    let para = getparams(params)
    return serve.get('/rs/show?' + para)
  },
  /* 导出excel报表 */
  exportAllExcel: (params) => serve.post('/rs/exportAllExcel', params),
  /* 获取报表名称 */
  getQueryInfo: (params) => {
    let para = getparams(params)
    return serve.get("/rs/jmreport/getQueryInfo?" + para)
  }
}

// /* 对象转url拼接 */
let getparams = (params) => {
  let str = ''
  let key = Object.keys(params)
  key.forEach((item, index) => {
    if (params[item]) {
      if (!index) {
        str += item + '=' + encodeURI(params[item])
      } else {
        str += '&' + item + '=' + encodeURI(params[item])
      }
    }
  })
  return str
}