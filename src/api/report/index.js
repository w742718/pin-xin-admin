/*
 * @Author: liaoxing
 * @Date: 2021-11-24 14:47:12
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-03-21 15:17:39
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\api\report\index.js
 */
/*
 * @Author: liaoxing
 * @Date: 2021-11-12 10:25:23
 * @LastEditors: liaoxing
 * @LastEditTime: 2021-11-24 14:40:04
 * @Description: liaoxing created
 * @FilePath: \data-report\src\jstar-dev\api\report.js
 */
import serve from '@/utils/report'
// let uri = process.env.NODE_ENV == 'production' ? '/report/api' : 'reportApi'
let uri = '/report/api'
export default {
  /* 新增报表 */
  addReport: () => serve.post('report/jmreport/save', {}),
  /* 查询目录树 */
  catalogue: () => serve.get(uri + '/v1/catalogue'),
  /* 新增目录 */
  addCatalogue: (params) => serve.post(uri + '/v1/catalogue', params),
  /* 删除目录 */
  deleteCatalogue: (id) => serve.delete(uri + '/v1/catalogue/' + id),
  /* 修改类别 */
  editCatalogue: (params) => {
    // let para = getparams(id)
    return serve.patch(uri + '/v1/catalogue/'+ params.id, params)
  },
  /* 查询报表 */
  // queryAllReportByType: () => serve.get('//' + uri + '/v1/report'),
  queryAllReportByType: (params) => {
    let para = getparams(params)
    return serve.get(uri + '/v1/report?' + para)
  },
  /* 搜索报表 */
  queryAllReportByName: (params) => {
    let para = getparams(params)
    return serve.get(uri + '/v1/report?' + para)
  },
  /* 查询报表参数 */
  queryParams: (id) => {
    return serve.get(uri + '/v1/report/' + id + '/params')
  },
  /* 查询报表详情 */
  queryReportById: (id) => serve.get(uri + '/v1/report?id=' + id),
  /* 报表授权 */
  authorizeReport: (id, userList) => serve.put(uri + '/v1/report/' + id + '/member', userList),
  /* 删除 */
  deleteReport:(id) => serve.delete(uri + '/v1/report/' + id),

  /* 报表目录 */
  userReport: () => serve.get(uri + '/v1/userReport'),
  /* 用户列表 */
  userList: () => serve.get('/rs/api/v1/user'),
  /* 员工列表 */
  memberList: (params) => {
    let para = getparams(params)
    return serve.get('/rs/api/v1/user?' + para)
  },

  /* 修改报表目录 */
  changeCategory: (id, params) => {
    return serve.patch(uri + '/v1/report/' + id, params)
  },
  /* 查询任务 */
  queryTask: (params) => {
    let para = getparams(params)
    return serve.get(uri + '/v1/reportPushTask?' + para)
  },
  /* 添加任务 */
  addTask: (params) => serve.post(uri + '/v1/reportPushTask', params),
  /* 修改任务 */
  editTask: (params) => serve.patch(uri + '/v1/reportPushTask/' + params.id, params.params),
  /* 删除任务 */
  deleteTask: (id) => serve.delete(uri + '/v1/reportPushTask/' + id),

  /* 财务报表 */
  /* 查询财务报表 */
  queryFinanceReport: (params) => serve.post(uri + '/v1/customReport/finance', params),
  /* 查询单据 */
  queryFinanceData: (params) => {
    let para = getparams(params)
    return serve.get(uri + '/v1/data/finance?' + para)
  },
  /* 添加录入单据 */
  addFinanceData: (params) => serve.post(uri + '/v1/data/finance', params),
  /* 删除单据 */
  deleteFinanceData: (id) => serve.delete(uri + '/v1/data/finance/' + id),
  /* 修改单据 */
  editFinanceData: (id, params) => serve.patch(uri + '/v1/data/finance/' + id, params),
  /* 数据重跑 */
  editFinanceRunData: (params) => serve.patch(uri + '/v1/data/finance', params),
}

// /api/v1/report/620897694519386112/member?userId=%5B1%2C1458607809820073986%2C1458607813599141889%5D
// /api/v1/report/620897694519386112/member?userId=%5B1%2C1458607809820073986%2C1458607813599141889%5D
// /* 对象转url拼接 */
let getparams = (params) => {
  let str = ''
  let key = Object.keys(params)
  key.forEach((item, index) => {
    if (params[item]) {
      if (!index) {
        str += item + '=' + encodeURI(params[item])
      } else {
        str += '&' + item + '=' + encodeURI(params[item])
      }
    }
  })
  return str
}