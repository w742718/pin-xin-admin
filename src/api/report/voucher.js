import serve from '@/utils/pinxin/xiaoxinyan'

const uri = process.env.NODE_ENV === 'development' ? '/xiaoxinyan' : '/report/api/v1'

export default {
  /* 查询凭证列表 */
  queryVoucherList:(params) =>{
    return serve.post('/report/api/v1/finance/list', params)
  }
}

// /* 对象转url拼接 */
let getparams = (params) => {
  let str = ''
  let key = Object.keys(params)
  key.forEach((item, index) => {
    if (params[item]) {
      if (!index) {
        str += item + '=' + encodeURI(params[item])
      } else {
        str += '&' + item + '=' + encodeURI(params[item])
      }
    }
  })
  return str
}