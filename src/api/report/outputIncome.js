/*
 * @Author: liaoxing
 * @Date: 2022-02-16 09:34:29
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-02-23 11:32:12
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\api\report\outputIncome.js
 */
import serve from '@/utils/pinxin/xiaoxinyan'

const uri = process.env.NODE_ENV === 'development' ? '/xiaoxinyan' : '/report/api/v1'

export default {
  /* excel上传 */
  // uploadExcel: (file) => serve.post('xiaoxinyan/reportOutputValueRevenue/upload', file, { headers: {'Content-Type': 'multipart/form-data'}),
  /* 查询产值和营收 */
  queryReportOutputValueRevenue: (params) => {
    return serve.post('/report/api/v1/reportOutputValueRevenue', params)
  },
  /* 查询进出口额 */
  queryReportImportExportVolume: (params) => {
    return serve.post('/report/api/v1/reportImportExportVolume', params)
  }
}

// /* 对象转url拼接 */
let getparams = (params) => {
  let str = ''
  let key = Object.keys(params)
  key.forEach((item, index) => {
    if (params[item]) {
      if (!index) {
        str += item + '=' + encodeURI(params[item])
      } else {
        str += '&' + item + '=' + encodeURI(params[item])
      }
    }
  })
  return str
}