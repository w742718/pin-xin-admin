/*
 * @Author: liaoxing
 * @Date: 2022-01-05 15:16:44
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-03-04 15:48:33
 * @Description: 报价api
 * @FilePath: \pin-xin-admin\src\api\quote\index.js
 */
import serve from '@/utils/quote'
import lixiaoping from '@/utils/report'
const uri = "/rs/api/v1/"
export default {
  /* 查询分类 */
  queryDictionary: () => serve.get("constant?_v=tree&isLeaf=0"),
  /* 添加分类 */
  addDictionary: (params) => serve.post("constantBundle", params.params),
  /* 删除 */
  deleteDictionary: (id) => serve.delete("materialSchedule/" + id),
  /* 修改 */
  editDictionary: (params) => serve.patch("materialSchedule/" + params.id, params.params),

  /* 查询收费项目 */
  queryItemExpenses: (params) => {
    const para = getparams(params)
    return serve.get("constant?" + para)
  },
  /* 添加收费项目 */
  addItemExpenses: (params) => serve.post("constant", params.params),
  /* 删除 */
  deleteItemExpenses: (id) => serve.delete("constant/" + id),
  /* 修改 */
  editItemExpenses: (params) => serve.patch("constant/" + params.id, params.params),

  /* 查询项目分类 chargingItemBundle */
  queryItemBundle: () => serve.get("expenseBundle"),
  /* 查询项目分类详情id */
  queryItemBundleById: (id) => serve.get("expenseBundle/" + id),
  /* 添加项目分类 */
  addItemBundle: (params) => serve.post("expenseBundle", params.params),
  /* 删除项目分类 */
  deleteItemBundle: (id) => serve.delete("expenseBundle/" + id),
  /* 修改项目分类 */
  editItemBundle: (params) => serve.patch("expenseBundle/" + params.id, params.params),

  /* 公式校验 */
  checkFormula: (params) => serve.post("expression", params),

  /* 查询收费项目chargingItemExpenses */
  queryChargingItemExpenses: (params) => {
    const para = getparams(params)
    return serve.get("expenseItem?" + para)
  },
  /* 添加收费项目 */
  addChargingItemExpenses: (params) => serve.post("expenseItem", params.params),
  /* 删除 */
  deleteChargingItemExpenses: (id) => serve.delete("expenseItem/" + id),
  /* 修改 */
  editChargingItemExpenses: (params) => serve.patch("expenseItem/" + params.id, params.params),

  /* 工程开机费计算接口 */
  calcValue: (id, params) => serve.post(`expenseItem/${id}/calcValue`, params),

  /* 工程报价提交 */
  // quotation/{id}/engineer 
  submitEngineer: (id, params) => serve.put(`quotation/${id}/engineer`, params),

  /* 下载文件 */
  downLoadFile: (id) => serve.get(`file/${id}`, {responseType: 'arraybuffer'}),

  // **************************************************************************
  /* 合作模式 */
  // getComboCoopMode: () => serve.get("comboCoopMode"),
  getComboCoopMode: () => lixiaoping.get(uri + "combo/coopMode"),

  /* 文件下载接口 */
  getDownFile: (params) => serve.get(`file/${params}`,),

  /* 采购提交接口 */
  getPurchase: (params) => serve.put(`quotation/${params.id}/purchase`,params.params),
  
  /* 查询客户 */
  // getComboCustomer: () => serve.get("comboCustomer"),
  getComboCustomer: () => lixiaoping.get(uri + "combo/customer"),

  /* 查询机型 产品编码 */
  getComboMaterial1: (params) => {
    const para = getparams(params)
    return lixiaoping.get(`${uri}/combo/productType?${para}`)
  },

  /* 查询辅料 */
  getComboMaterial2: () => serve.get("comboMaterial?type=A2&materialCode_like=1"),

  /* 查询工治具 */
  getComboMaterial4: () => serve.get("comboMaterial?type=A4&materialCode_like=1"),

  /* 查询报价单 */
  getQuotation: (params) => serve.get("quotation",params),

  /* 旧添加报价单 */
  postQuotation: (params) => serve.post("quotation", params),
    /* ***************************************************** */




  /* 查询报价单基本信息 */
  // getBaseQuotation: (params) => serve.get(`quotation/${params}/base`),
  getBaseQuotation: (params) => serve.get(`chargeItem/${params}`),

  /* 成本清单 */
  // getCost: (params) => serve.get(`quotation/${params}/cost`),
  getCost: (params) => lixiaoping.get(`${uri}/costQuote/${params}/efficiency`),

  /* 固定成本清单 */
  getFactCost: (params) => serve.get(`factCost`),

  /* 查询报价单开机费 */
  getMachineQuotation: (params) => serve.get(`quotation/${params}/machine`),

  /* 查询报价单加工费  效率清单*/
  getEfficiency: (params) => serve.get(`quotation/${params}/efficiency`),

  /* 查询客供辅料 */
  getMaterial: (params) => serve.get(`quotation/${params}/material?type=slaveMaterial&materialFrom=1`),

  /*  查询代采辅料 辅料 */
  getSlaveMaterial0: (params) => serve.get(`quotation/${params}/material?type=slaveMaterial&materialFrom=0`),

  /* 查询客供工治具 */
  getFixture1: (params) => serve.get(`quotation/${params}/material?type=fixture&materialFrom=1`),

  /* 查询代采工治具 工治具*/
  getFixture0: (params) => serve.get(`quotation/${params}/material?type=fixture&materialFrom=0`),

  /* 查询客供物料 */
  getMasterMaterial1: (params) => serve.get(`quotation/${params}/material?type=masterMaterial&materialFrom=1`),
    
    /* 查询代采物料 */
  getMasterMaterial0: (params) => serve.get(`quotation/${params}/material?type=masterMaterial&materialFrom=0`),


  /* 查询辅料物料信息 */
  getSlaveMaterialList: () => serve.get("comboMaterial?type=A2&materialCode_like=1&clsId=1&pn=1&ps=9999"),

  /* 查询工治具物料信息 */
  getFixtureList: () => serve.get("comboMaterial?type=A4&materialCode_like=1&clsId=1&pn=1&ps=9999"),

  /* ***************************************************** */





  /* 获取待办任务 */
  getUpcomingTask: () => lixiaoping.get(uri + "upcomingTask"),
  /* 获取待办任务详情 */
  getUpcomingTaskById: (id) => lixiaoping.get(uri + "upcomingTask/" + id),
  /* 提交任务 */
  putUpcomingTask: (params) => lixiaoping.put(uri + "upcomingTask/" + params.id, params),

  /* 暂存任务 */
  postUpcomingTask: (id) => lixiaoping.post(uri + "upcomingTask/" + id),
  
  /* ********************************************************** */
  /* ********************************************************** */
  /* ********************************************************** */

  /* 客户列表 */
  queryCustomerList: (params) => {
    let para = getparams(params)
    return lixiaoping.get(uri + "custQuote?" + para)
  },
  /* 客户报价信息 */
  queryCustomerInfo: (id) => lixiaoping.get(`${uri}custQuote/${id}`),
  /* 提交报价信息 */
  putCustomerInfo: (id, params) => lixiaoping.put(`${uri}custQuote/${id}`, params) 
}

/* 对象转url拼接 */
let getparams = (params) => {
  let str = ''
  let key = Object.keys(params)
  key.forEach((item, index) => {
    if (params[item]) {
      if (!index) {
        str += item + '=' + encodeURI(params[item])
      } else {
        str += (str ? '&' : '') + item + '=' + encodeURI(params[item])
      }
    }
  })
  return str
}