/*
 * @Author: liaoxing
 * @Date: 2022-01-14 14:59:31
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-03-07 15:33:25
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\api\quote\params.js
 */
import serve from '@/utils/params'
import lixiaoping from '@/utils/report'

const uri = "/rs/api/v1/"

export default {
  /* 获取参数类型 */
  getParameterType: () => serve.get("combo/parameterType"),
  /* 获取数据来源 */
  getParameterValueFrom: () => serve.get("combo/parameterValueFrom"),
  /* 获取工治具 */
  getToolMaterial: (params) => {
    const para = getparams(params)
    return lixiaoping.get(uri + "combo/toolMaterial?" + para)
  },
  /* 获取合作主体 */
  getOrganization: () => lixiaoping.get(uri + "combo/organization"),


  /* 查询参数分页 */
  queryDictionaryAsPage: (id) => lixiaoping.get(uri + "parameter?floderId=" + id),
  /* 查询参数列表 */
  queryDictionaryAsList: (params) => {
    let para = getparams(params)
    return lixiaoping.get(uri + "parameter?" + para)
  },
  queryDictionaryDetail: (id) => lixiaoping.get(uri + "parameter/" + id),
  /* 查询参数树 */
  // queryDictionaryAsTree: () => lixiaoping.get("parameter?_v=tree"),
  /* 查询无组合参数树 */
  queryDictionaryAsTreeWithoutComb: () => lixiaoping.get(uri + "parameter?_v=tree&parameterType=BASE"),
  /* 添加参数 */
  addDictionary: (params) => lixiaoping.post(uri + "parameter", params.params),
  /* 删除 */
  deleteDictionary: (id) => lixiaoping.delete(uri + "parameter/" + id),
  /* 修改 */
  editDictionary: (params) => lixiaoping.patch(uri + "parameter/" + params.id, params.params),


  /* 添加分类 */
  addParemtFld: (params) => lixiaoping.post(uri + "parameterFolder", params),
   /* 修改分类 */
  editParemtFld: (params) => lixiaoping.patch(uri + `parameterFolder/`+ params.id, params),
   /* 删除分类 */
  deleParemtFld: (id) => lixiaoping.delete(uri + "parameterFolder/" + id),
  /* 查询分类 */
  queryParemtFld: () => {
    return lixiaoping.get(uri + "parameterFolder?_v=tree")
  },

  /* 获取物料配置 */
  getMaterialConfig: (id) => serve.get("materialConfigure/" + id),
  /*  */
  /* 新添加报价单 */
  newQuotation: (params) => serve.post("costQuote",params),


  /* 查询报价单 */
  // gCostQuote: (params) => serve.get(`costQuote`, + params),
  gCostQuote: (params) => {
    let para = getparams(params)
    return lixiaoping.get(`${uri}/costQuote?` + para)
  },


  /* 新报价单列表 */
  pCostQuote: (params) => lixiaoping.post(uri + "costQuote", params),
  /* 查询报价单 */
  costQuoteDetail: (id) => lixiaoping.get(uri + "costQuote/" + id),

  /* 查询收费分类 */
  queryItemBundle: () => serve.get("chargeFolder"),
  /* 查询收费分类树 */
  queryItemBundleTree: () => serve.get("chargeFolder?_v=tree"),
  /* 添加收费分类 */
  addItemBundle: (params) => serve.post("chargeFolder/", params.params),
  /* 删除收费分类 */
  deleteItemBundle: (id) => serve.delete("chargeFolder/"+ id),
  /* 修改收费分类 */
  editItemBundle: (params) => serve.patch("chargeFolder/"+ params.id, params.params),
  /* 查询收费项分类详情 */
  queryItemBundleById: (id) => serve.get("chargeFolder/" + id),



  /* 查询收费项目*/
  queryChargingItemExpenses: (params) => {
    let para = getparams(params)
    return lixiaoping.get(uri + "chargeItem?" + para)
  },
  /* 添加收费项目 */
  addChargingItemExpenses: (params) => lixiaoping.post(uri + "chargeItem", params),
  /* 修改 */
  editChargingItemExpenses: (params) => lixiaoping.patch(uri + "chargeItem/" + params.id, params.params),
  /* 删除 */
  deleteChargingItemExpenses: (id) => lixiaoping.delete(uri + "chargeItem/" + id),
  /* 查询参数树状 */
  queryDictionaryAsTree: (id) => lixiaoping.get(uri + "parameter?_v=tree"),
  /* 公式 */
  checkFormula1: (params) => lixiaoping.post(uri + "query/calculatorArgument", params),
  /* 公式校验 */
  checkFormula: (params) => lixiaoping.post(uri + "calculator", params),

  /* ********************************************************************** */
  /* ********************************************************************** */
  /* ****************************** 年后分割符  ******************************/
  /* ********************************************************************** */
  /* ********************************************************************** */

  /* 参数值获取 */
  queryParameterValue: (id) => lixiaoping.get(uri + "parameter/" + id + "/value"),
  /* 参数值添加 */
  addParameterValue: (id, params) => lixiaoping.put(uri + "parameter/" + id + "/value", params),
  /* 参数值删除 */
  deleteParameterValue: (id, vid) => lixiaoping.delete(uri + "parameter/" + id + "/value/" + vid),
  

  addDbConnect: (params) => serve.post(`db`, params),
}

/* 对象转url拼接 */
let getparams = (params) => {
  let str = ''
  let key = Object.keys(params)
  key.forEach((item, index) => {
    if (params[item]) {
      if (!index) {
        str += item + '=' + encodeURI(params[item])
      } else {
        str += (str ? '&' : '') + item + '=' + encodeURI(params[item])
      }
    }
  })
  return str
}