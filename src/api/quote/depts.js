
import dbParams from '@/utils/dbParams'
export default {
  /* 获取部门 */
  getDepts: () => dbParams.get("dept"),
  
  /* 添加部门 */
  postDepts: (params) => dbParams.post("dept",params),

  /* 修改部门 */
  patchDepts: (params) => dbParams.patch("dept/" + params.id, params),
  
  /* 删除部门 */
  deleDepts: (params) => dbParams.delete("dept/" + params.id,),



  /* 获取角色 */
  getRoles: (params) => {
    let para = getparams(params)
    return dbParams.get("role?" + para)
  }, 
  
  // /* 获取菜单栏 */
  // getUser: (params) => {
  //   let para = getparams(params)
  //   return dbParams.get("user?" + para)
  // },
  
  /* 添加角色 */
  postRoles: (params) => dbParams.post("role",params),

  /* 修改角色 */
  patchRoles: (params) => dbParams.patch("role/" + params.id, params),
  
  /* 删除角色 */
  deleRoles: (params) => dbParams.delete("role/" + params.id,),

  /* 角色授权 */
  putRoles: (params) => dbParams.put(`role/${params.id}/menu`, params.params),

  

   /* 获取员工 */
  getUser: (params) => {
    let para = getparams(params)
    return dbParams.get("user?" + para)
  }, 

   /* 添加员工 */
  postUser: (params) => dbParams.post("user",params),
 
   /* 修改员工 */
  patchUser: (params) => dbParams.patch("user/" + params.id, params),
   
   /* 删除员工 */
  deleUser: (params) => dbParams.delete("user/" + params.id,),
   


   /* 添加账号 */
  postAccount: (params) => dbParams.post("account", params),
  
  /* 获取账号 */
  getAccount: (params) => {
    let para = getparams(params)
    return dbParams.get("Account?" + para)
  }, 

  /* 修改账号 */
  patchAccount: (params) => dbParams.patch("Account/" + params.id, params),
   
   /* 删除账号 */
  deleAccount: (params) => dbParams.delete("Account/" + params.id,),



  /* 获取菜单 */
  getMenu: (params) => {
    let para = getparams(params)
    return dbParams.get("menu?" + para)
  }, 

  /* 添加菜单 */
  postMenu: (params) => dbParams.post("menu",params),

  /* 修改菜单 */
  patchMenu: (params) => dbParams.patch("menu/" + params.id, params),
  
  /* 删除菜单 */
  deleMenu: (params) => dbParams.delete("menu/" + params.id,),




}

/* 对象转url拼接 */
let getparams = (params) => {
  let str = ''
  let key = Object.keys(params)
  key.forEach((item, index) => {
    if (params[item]) {
      if (!index) {
        str += item + '=' + encodeURI(params[item])
      } else {
        str += (str ? '&' : '') + item + '=' + encodeURI(params[item])
      }
    }
  })
  return str
}