/*
 * @Author: liaoxing
 * @Date: 2022-01-14 14:59:31
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-03-02 18:12:01
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\api\task\index.js
 */
import serve from '@/utils/params'
import lixiaoping from '@/utils/report'

const uri = "/rs/api/v1/"

export default {
  /* 获取待办任务 */
  getUpcomingTask: (params) => {
    const para = getparams(params)
    return lixiaoping.get(uri + "upcomingTask?" + para)
  },
  /* 获取待办任务详情 */
  getUpcomingTaskById: (id) => lixiaoping.get(uri + "upcomingTask/" + id),
  /* 提交任务 */
  putUpcomingTask: (id, params) => lixiaoping.put(uri + "upcomingTask/" + id, params),
  /* 暂存任务 */
  postUpcomingTask: (id, params) => lixiaoping.post(uri + "upcomingTask/" + id, params),
  /* 删除任务 */
  deleteUpcomingTask: (id) => lixiaoping.post(uri + "upcomingTask/" + id),


}

/* 对象转url拼接 */
let getparams = (params) => {
  let str = ''
  let key = Object.keys(params)
  key.forEach((item, index) => {
    if (params[item]) {
      if (!index) {
        str += item + '=' + encodeURI(params[item])
      } else {
        str += (str ? '&' : '') + item + '=' + encodeURI(params[item])
      }
    }
  })
  return str
}