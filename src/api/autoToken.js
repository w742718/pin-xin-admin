
import serve from '@/utils/report'
// let uri = process.env.NODE_ENV == 'production' ? '/system/api' : '/systemApi'
let uri = '/system/api'
let url = '/rs/api/v1'
/* 获取access_token */
export function getAccessToken(code) {
  return serve({
    url: uri + '/v1/token',
    method: 'get',
    params: {code: code}
  })
}

// 获取用户详细信息
export function getInfos() {
  return serve({
    url: uri + '/v1/user/getInfo',
    method: 'get',
  })
}


// 获取路由
export const getRouters = (id = 0) => {
  return serve({
    url: uri + '/v1/user/' + id + '/menu',
    method: 'get'
  })
}

// 获取新路由
export const getRouter = (id = 0) => {
  return serve({
    url: url + '/user/' + id + '/menu',
    method: 'get'
  })
}



