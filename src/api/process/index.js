/*
 * @Author: liaoxing
 * @Date: 2021-10-25 11:34:01
 * @LastEditors: liaoxing
 * @LastEditTime: 2021-11-04 10:53:07
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\api\process\index.js
 */
import serve from '@/utils/serve'

export default {
  /* 分类列表 */
  getCategories: (params) => {
    let para = getparams(params)
    return serve.get('/bpmn/category?' + para)
  },
  /* 保存更新分类 */
  saveOrUpdate: (params, type, id) => {
    if (type) {
      return serve.patch('/bpmn/category/' + id, params)
    }
    return serve.post('/bpmn/category', params)
  },
  /* 删除分类 */ // 传参意思，返回啥
  delete: (params) => serve.delete('/bpmn/category/' +  params),
  /* 检测模型是否存在 */
  checkEntityExiste: (params) => serve.get('/bpmn/model-form/tag?tagName=' + params),
  /* 流程列表 */
  getPagerModel: (params) => {
    let para = getparams(params)
    return serve.get('/bpmn/model-form?' + para)
  },
  /* 删除流程 */
  deleteByIds: (params) => serve.delete('/bpmn/model-form/' + params),
  /* 流程新增或者修改 */
  saveOrUpdateModelInfo: (params, type, id) => {
    if (type) { // 修改
      return serve.patch('/bpmn/model-form/' + id, params)
    }
    return serve.post('/bpmn/model-form', params)
  },
  /* 流程发布 */
  publishBpmn: (params) => {
    return serve.post('/bpmn/repository/deployments/' + params)
  },
  /* 流程停用 */
  stopBpmn: (params) => {
    return serve.delete('/bpmn/repository/deployments/' + params)
  },
  /* 获取模型信息 */
  getByModelId: (params) => serve.get('/bpmn/repository/models/' + params),
  // /* 查询模型 */
  // getBpmnByModelId: (params) => serve.get('/flow/bpmnDesigner/open/api/getBpmnByModelId/' + params),
  /* 校验模型 */
  validateBpmnModell: (params) => serve.post('/flow/bpmnDesigner/open/api/validateBpmnModell', params),
  /* 保存模型 */
  saveBpmnModel: (params) => serve.post('/bpmn/repository/models/content', params),
  /* 模型预览 */
  getBpmnByModelKey: (params) => {
    let para = getparams(params)
    return serve.get('/flow/flowable/bpmn/getBpmnByModelKey/induction?' + para)
  } 
}

let old = {
  login: (params) => serve.post('/processLogin', params),
  /* 分类列表 */ // 缺少分页，好有模糊查询，返回的字段意思
  getCategories: (params) => serve.post('/flow/base/category/getCategories', params),
  /* 保存更新分类 */ // 传参字段意思，返回啥
  saveOrUpdate: (params) => serve.post('/flow/base/category/saveOrUpdate', params),
  /* 检测分类是否存在 */
  checkEntityExiste: (params) => serve.post('/flow/base/category/checkEntityExist', params),
  /* 删除分类 */ // 传参意思，返回啥
  delete: (params) => serve.post('/flow/base/category/delete', params),
  /* 所属系统 */
  getApps: (params) => serve.post('/flow/base/app/getApps', params),
  /* 检查流程是否存在 */
  checkEntityExist: (params) => serve.post('/flow/flowable/modelInfo/checkEntityExist', params),
  /* 流程列表 */
  getPagerModel: (params) => serve.post('/flow/flowable/modelInfo/getPagerModel', params),
  /* 删除流程 */
  deleteByIds: (params) => serve.post('/flow/flowable/modelInfo/deleteByIds', params),
  /* 流程新增或者修改 */
  saveOrUpdateModelInfo: (params) => serve.post('/flow/flowable/modelInfo/saveOrUpdateModelInfo', params),
  /* 流程发布 */
  publishBpmn: (params) => {
    return serve.post('/flow/flowable/bpmn/publishBpmn/' + params)
  },
  /* 流程停用 */
  stopBpmn: (params) => {
    return serve.post('/flow/flowable/bpmn/stopBpmn/' + params)
  },
  /* 获取模型信息 */
  getByModelId: (params) => serve.get('/flow/flowable/modelInfo/getByModelId/' + params + '?_t=' + new Date().getTime()),
  /* 查询模型 */
  getBpmnByModelId: (params) => serve.get('/flow/bpmnDesigner/open/api/getBpmnByModelId/' + params),
  /* 校验模型 */
  validateBpmnModell: (params) => serve.post('/flow/bpmnDesigner/open/api/validateBpmnModell', params),
  /* 保存模型 */
  saveBpmnModel: (params) => serve.post('/flow/bpmnDesigner/open/api/saveBpmnModel', params),
  /* 模型预览 */
  getBpmnByModelKey: (params) => {
    let para = getparams(params)
    return serve.get('/flow/flowable/bpmn/getBpmnByModelKey/induction?' + para)
  } 
}

/* 对象转url拼接 */
let getparams = (params) => {
  let str = ''
  let key = Object.keys(params)
  key.forEach((item, index) => {
    if (!index) {
      str += item + '=' + params[item]
    } else {
      str += '&' + item + '=' + params[item]
    }
  })
  return str
}