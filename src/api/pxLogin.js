/*
 * @Author: liaoxing
 * @Date: 2022-03-17 10:33:23
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-03-17 10:36:50
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\api\pxLogin.js
 */
import serve from '@/utils/report'

// /rs/api/v1/token
export default {
  /* 登入 */
  pxLogin: (params) => serve.post('/rs/api/v1/token', params)
}