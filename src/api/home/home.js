import request from '@/utils/request'

 //订单量
export function getStatistcs(query) {
  return request({
    url: 'http://192.168.12.150:8008/api/v1/order/statistics',
    method: 'get',
    params: query
  })
}

// 订单列表
export function getOrderList(query) {
  return request({
    url: 'http://192.168.12.150:8008/api/v1/order/list',
    method: 'get',
    params: query
  })
}

//订单流程
export function getFlowlist(query) {
  return request({
    url: 'http://192.168.12.86:8006/api/v1/order/flowlist',
    method: 'get',
    params: query
  })
}

//订单详情
export function getOrderProtop(query) {
  return request({
    url: 'http://192.168.12.86:8006/api/v1/order/protop',
    method: 'get',
    params: query
  })
}


//订单详情第一步
export function getOrderDetail(query) {
  return request({
    url: 'http://192.168.12.86:8006/api/v1/order/detail',
    method: 'get',
    params: query
  })
}

//查看工单明细
export function getProlist(query) {
  return request({
    url: 'http://192.168.12.86:8006/api/v1/order/prolist',
    method: 'get',
    params: query
  })
}


//查看工单明细详情
export function getProdetail(query) {
  return request({
    url: 'http://192.168.12.86:8006/api/v1/order/prodetail',
    method: 'get',
    params: query
  })
}


//生产报警
export function getAlertTypeName(query) {
  return request({
    url: 'http://192.168.12.86:8006/api/alert/head/queryByAlertTypeName',
    method: 'get',
    params: query
  })
}








// 根据参数键名查询参数值
export function getConfigKey(configKey) {
  return request({
    url: '/api/v1/config/configKey/' + configKey,
    method: 'get'
  })
}

// 新增参数配置
export function addConfig(data) {
  return request({
    url: '/dev-api/system/config',
    method: 'post',
    data: data
  })
}

// 修改参数配置
export function updateConfig(data) {
  return request({
    url: '/dev-api/system/config',
    method: 'put',
    data: data
  })
}

// 删除参数配置
export function delConfig(configId) {
  return request({
    url: '/dev-api/system/config/' + configId,
    method: 'delete'
  })
}

// 刷新参数缓存
export function refreshCache() {
  return request({
    url: '/dev-api/system/config/refreshCache',
    method: 'delete'
  })
}
