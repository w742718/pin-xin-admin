/*
 * @Author: liaoxing
 * @Date: 2022-03-21 14:46:13
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-03-22 10:06:55
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\api\system\account.js
 */
import serve from '@/utils/report'

const uri = "/rs/api/v1/"

export default {
  /* 查询分页 */
  queryAccount: (params) => {
    const para = getparams(params)
    return serve.get(uri + "account?" + para)
  },
  addAccount: (params) => serve.post(uri + "account", params),
  editAccount: (id, params) => serve.patch(uri + "account/" + id, params),
  deleteAccount: (id) => serve.delete(uri + "account/" + id),
  /* 修改密码 */
  changePassword: (id, params) => serve.put(`${uri}account/${id}/password`, params),
  /* 重置密码 */
  resetPassword: (id) => serve.delete(`${uri}account/${id}/password`)
}

/* 对象转url拼接 */
let getparams = (params) => {
  let str = ''
  let key = Object.keys(params)
  key.forEach((item, index) => {
    if (params[item]) {
      if (!index) {
        str += item + '=' + encodeURI(params[item])
      } else {
        str += (str ? '&' : '') + item + '=' + encodeURI(params[item])
      }
    }
  })
  return str
}