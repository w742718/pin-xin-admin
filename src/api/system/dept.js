/*
 * @Author: liaoxing
 * @Date: 2021-11-24 10:10:51
 * @LastEditors: liaoxing
 * @LastEditTime: 2021-12-04 10:54:07
 * @Description: liaoxing created
 * @FilePath: \品新集成管理系统\pin-xin-admin\src\api\system\dept.js
 */
import request from '@/utils/report'
// let uri = process.env.NODE_ENV == 'production' ? '/system/api' : '/systemApi'
let uri = '/system/api'
// 查询部门列表
export function listDept(query) {
  return request({
    url: '/dev-api/system/dept/list',
    method: 'get',
    params: query
  })
}

// 查询部门列表（排除节点）
export function listDeptExcludeChild(deptId) {
  return request({
    url: '/dev-api/system/dept/list/exclude/' + deptId,
    method: 'get'
  })
}

// 查询部门详细
export function getDept(deptId) {
  return request({
    url: '/dev-api/system/dept/' + deptId,
    method: 'get'
  })
}

// 查询部门下拉树结构
export function treeselect() {
  return request({
    url: uri + '/v1/dept',
    method: 'get'
  })
}

// 根据角色ID查询部门树结构
export function roleDeptTreeselect(roleId) {
  return request({
    url: '/dev-api/system/dept/roleDeptTreeselect/' + roleId,
    method: 'get'
  })
}

// 新增部门
export function addDept(data) {
  return request({
    url: '/dev-api/system/dept',
    method: 'post',
    data: data
  })
}

// 修改部门
export function updateDept(data) {
  return request({
    url: '/dev-api/system/dept',
    method: 'put',
    data: data
  })
}

// 删除部门
export function delDept(deptId) {
  return request({
    url: '/dev-api/system/dept/' + deptId,
    method: 'delete'
  })
}