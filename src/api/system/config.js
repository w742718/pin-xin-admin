/*
 * @Author: liaoxing
 * @Date: 2021-11-24 10:10:51
 * @LastEditors: liaoxing
 * @LastEditTime: 2021-11-26 13:58:36
 * @Description: liaoxing created
 * @FilePath: \品新集成管理系统\pin-xin-admin\src\api\system\config.js
 */
import request from '@/utils/request'

// 查询参数列表
export function listConfig(query) {
  return request({
    url: '/dev-api/system/config/list',
    method: 'get',
    params: query
  })
}

// 查询参数详细
export function getConfig(configId) {
  return request({
    url: '/dev-api/system/config/' + configId,
    method: 'get'
  })
}

// 根据参数键名查询参数值
export function getConfigKey(configKey) {
  return request({
    url: '/api/v1/config/configKey/' + configKey,
    method: 'get'
  })
}

// 新增参数配置
export function addConfig(data) {
  return request({
    url: '/dev-api/system/config',
    method: 'post',
    data: data
  })
}

// 修改参数配置
export function updateConfig(data) {
  return request({
    url: '/dev-api/system/config',
    method: 'put',
    data: data
  })
}

// 删除参数配置
export function delConfig(configId) {
  return request({
    url: '/dev-api/system/config/' + configId,
    method: 'delete'
  })
}

// 刷新参数缓存
export function refreshCache() {
  return request({
    url: '/dev-api/system/config/refreshCache',
    method: 'delete'
  })
}
