/*
 * @Author: liaoxing
 * @Date: 2021-11-24 10:10:51
 * @LastEditors: liaoxing
 * @LastEditTime: 2021-12-04 10:25:43
 * @Description: liaoxing created
 * @FilePath: \品新集成管理系统\pin-xin-admin\src\api\system\menu.js
 */
import request from '@/utils/report'
// let uri = process.env.NODE_ENV == 'production' ? '/system/api' : '/systemApi'
let uri = '/system/api'
// 查询菜单列表
export function listMenu(query) {
  return request({
    url: uri + '/v1/menu',
    method: 'get',
    params: query
  })
}

// 查询菜单详细
export function getMenu(menuId) {
  return request({
    url: uri + '/v1/menu/' + menuId,
    method: 'get'
  })
}

// 查询菜单下拉树结构
export function treeselect() {
  return request({
    url: uri + '/v1/menu/treeselect',
    method: 'get'
  })
}

// 根据角色ID查询菜单下拉树结构
export function roleMenuTreeselect(roleId) {
  return request({
    url: uri + '​/v1​/menu​/roleMenuTreeselect​/' + roleId,
    method: 'get'
  })
}

// 新增菜单
export function addMenu(data) {
  return request({
    url: uri + '/v1/menu',
    method: 'post',
    data: data
  })
}

// 修改菜单
export function updateMenu(data) {
  return request({
    url: uri + '/v1/menu/' + data.id,
    method: 'patch',
    data: data
  })
}

// 删除菜单
export function delMenu(menuId) {
  return request({
    url: uri + '/v1/menu/' + menuId,
    method: 'delete'
  })
}