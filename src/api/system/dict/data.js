/*
 * @Author: liaoxing
 * @Date: 2021-11-24 10:10:51
 * @LastEditors: liaoxing
 * @LastEditTime: 2021-12-03 18:13:57
 * @Description: liaoxing created
 * @FilePath: \品新集成管理系统\pin-xin-admin\src\api\system\dict\data.js
 */
import request from '@/utils/request'
let uri = process.env.NODE_ENV == 'production' ? '/system/api' : '/systemApi'
// 查询字典数据列表
export function listData(query) {
  return request({
    url: '/dev-api/system/dict/data/list',
    method: 'get',
    params: query
  })
}

// 查询字典数据详细
export function getData(dictCode) {
  return request({
    url: '/dev-api/system/dict/data/' + dictCode,
    method: 'get'
  })
}

// 根据字典类型查询字典数据信息
export function getDicts(dictType) {
  return request({
    url: uri +'/v1/sysDict/' + dictType,
    method: 'get'
  })
}

// 新增字典数据
export function addData(data) {
  return request({
    url: '/dev-api/system/dict/data',
    method: 'post',
    data: data
  })
}

// 修改字典数据
export function updateData(data) {
  return request({
    url: '/dev-api/system/dict/data',
    method: 'put',
    data: data
  })
}

// 删除字典数据
export function delData(dictCode) {
  return request({
    url: '/dev-api/system/dict/data/' + dictCode,
    method: 'delete'
  })
}
