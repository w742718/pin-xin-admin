import serve from '@/utils/dbParams'
import serves from '@/utils/dbParam'
export default {
  //添加数据库连接
  addDbConnect: (params) => serve.post(`db`, params),

  // 获取数据库所有表
  getDbList: (id) => serve.get(`db/${id}/table`),

  //查询数据库列表
  getDbTable: () => serve.get(`db`),

  //删除数据库
  delDbTable: (params) => serve.delete(`db/${params.id}`),

  //修改数据库
  patchDbTable: (params) => serve.patch(`db/${params.id}`,params.params),

  //查询表信息
  getTableMsg: (params) => serve.get(`db/${params.id}/table/${params.params}`),

  //查询数字展示
// getShowTable: (params) => serve.get(`db/${params.id}/table/${params.params}/data`, params.page),
  
  getShowTable: (params,paras) => {
    let para = getparams(params)
    return serve.get(`db/${paras.id}/table/${paras.params}/data?` + para)
  }, 

  //添加
  putTableMsg: (params) => serve.put(`db/${params.id}/table/${params.params}`, params.dataFile),
  
  //查询
  getSqlTable: (params) => serve.post(`db/${params.id}/sql`,params.params),

  /* 公司组织列表查询 */
  queryErpTableExt: (params) => {
    let para = getparams(params)
    return serve.get("erpTableExt/erp_dim_org?" + para)
  },
  /* 获取辅料 */
  getAccessoriesType: (params) => {
    let para = getparams(params)
    return serve.get("combo/assistMaterial?" + para)
  },
  /* 添加辅料 */
  addAccessories: (params) => serve.put(`erpTableExt/${params.tableName}/row/${params.primaryKey}`,params.params),
  /* 获取绑定辅料 */
  getBindAccessories: (params) => serve.get(`erpTableExt/${params.tableName}/row/${params.primaryKey}/column/${params.columnKey}`),

  getSqlTable: (params) => serve.post(`db/${params.id}/sql`, params.params),
  
 // ### 数据库连接测试
 // getDbTest: (params) => serve.post(`dbTest`, params),
 // ### 数据库连接测试
  getDbTest: (params) => serves.post(`testConnection `, params),
  
  //添加修改数据库连接
  addDbTest: (params) => serves.post(`addDataSource `, params),
  

 
}
/* 对象转url拼接 */
let getparams = (params) => {
  let str = ''
  let key = Object.keys(params)
  key.forEach((item, index) => {
    if (params[item]) {
      if (!index) {
        str += item + '=' + encodeURI(params[item])
      } else {
        str += (str ? '&' : '') + item + '=' + encodeURI(params[item])
      }
    }
  })
  return str
}