/*
 * @Author: liaoxing
 * @Date: 2022-01-05 15:20:07
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-02-08 15:30:15
 * @Description: liaoxing created
 * @FilePath: \品新集成管理系统\pin-xin-admin\src\utils\quote.js
 */
import axios from 'axios'
// import sessionStorage from '@/utils/sessionStorage'
import cookies from '@/utils/cookies'
import store from '@/store'
// let url = process.env.NODE_ENV == 'production' ? window.baseURL : '/'
const serve = axios.create({
  timeout: 1000000,
  baseURL: window.baseURL + "/saas/api/v1/"
})
serve.interceptors.request.use(
  config => {
      let token = cookies.getCookie('userInfo')
      if (token) {
        config.headers['access_token']  = token.accessToken
      }
      return config
  },
  error => {
      Promise.reject(error)
  }
)

serve.interceptors.response.use(
  response => {
      const res = response.data
      if (response.code === 403 || response.code === 401) {
        // store.dispatch('FedLogOut').then(() => {
        //   location.href = '/login'
        // })
      }
      
      if (response.config.responseType == "arraybuffer") {
        return response
      } else {
        return res
      }
  },
  error => {
    console.log(error)
      if (error.response.status > 400 && error.response.status < 500) {
        // store.dispatch('FedLogOut').then(() => {
        //   location.href = '/login'
        // })
      }
      return Promise.reject(error)
  }
)

export default serve