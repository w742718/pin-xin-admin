/*
 * @Author: liaoxing
 * @Date: 2021-11-24 14:48:32
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-03-04 15:40:32
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\utils\report.js
 */
import axios from "axios"
// import sessionStorage from '@/utils/sessionStorage'
import cookies from '@/utils/cookies'
import store from '@/store'
// let url = process.env.NODE_ENV == 'production' ? window.baseURL : '/'
const serve = axios.create({
  timeout: 100000,
  baseURL: window.baseURL
})
serve.interceptors.request.use(
  config => {
      let token = cookies.getCookie('userInfo')
      if (token) {
        config.headers['access_token']  = token.accessToken
      }
      return config
  },
  error => {
      Promise.reject(error)
  }
)

serve.interceptors.response.use(
  response => {
      const res = response.data
      if (response.code === 403 || response.code === 401) {
        // store.dispatch('FedLogOut').then(() => {
        //   location.href = '/login'
        // })
      }
      return res
  },
  error => {
    console.log(error)
      if (error.response.status > 400 && error.response.status < 500) {
        // store.dispatch('FedLogOut').then(() => {
        //   location.href = '/login'
        // })
      }
      return Promise.reject(error)
  }
)

export default serve