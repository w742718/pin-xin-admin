/*
 * @Author: liaoxing
 * @Date: 2021-11-24 10:10:52
 * @LastEditors: liaoxing
 * @LastEditTime: 2021-12-15 10:28:42
 * @Description: liaoxing created
 * @FilePath: \品新集成管理系统\pin-xin-admin\src\utils\serve.js
 */
import axios from 'axios'
import cookies from '@/utils/cookies'
import sessionStorage from '@/utils/sessionStorage'
const serve = axios.create(
  {
    timeout: 10000,
    // baseURL: process.env.NODE_ENV === 'production' ? window.baseURL : '/',
    baseURL: window.baseURL
  }
)

serve.interceptors.request.use(
  async config => {T
    let token = cookies.getCookie('userInfo')
    if (token) {
      config.headers['access_token']  = token.accessToken
    }
  },
  error => {
      Promise.reject(error)
  }
)

serve.interceptors.response.use(
  response => {
      const res = response.data
      return res
  },
  error => {
      return Promise.reject(error)
  }
)

function autoLogin () {
  return new Promise(function (res, rej) {
    axios.post('/processmanage', {"username": "10100", "password": "888888"})
    .then(function(result) {
      if (result.status === 200) {
        let data = result.data
        if (data.code === "100") {
          cookies.setCookie("SESSION_ID", data.data, "Session")
          res()
        } else {
          rej()
        }
      } else {
        rej()
      }
    })
    .catch(e => {
      rej()
      console.log(e)
    })
  })
}

export default serve