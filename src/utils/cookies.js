/*
 * @Author: liaoxing
 * @Date: 2021-10-25 15:56:47
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-03-15 10:29:06
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\utils\cookies.js
 */
export default {
  setCookie (name, value, time) {
    var Days = 30
    var exp = new Date()
    exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 30)
    document.cookie = name + '=' + escape(JSON.stringify(value)) + ';expires=' + (time || exp.toGMTString())
  },
  getCookie (name) {
    let cookieReg = new RegExp('(^| )' + name + '=([^;]*)(;|$)')
    let cookieArray = document.cookie.match(cookieReg)
    if (cookieArray) {
      return JSON.parse(unescape(cookieArray[2]))
    } else {
      return null
    }
  },
  getCookieName (name) {
    let cookieReg = new RegExp('(^| )' + name + '=([^;]*)(;|$)')
    let cookieArray = document.cookie.match(cookieReg)
    if (cookieArray) {
      return (cookieArray[2])
    } else {
      return null
    }
  },
  deleteCookie (name) {
    let date = new Date()
    date.setTime(date.getTime() - 10000)
    document.cookie = name + '=0;expires=' + date.toUTCString()
  },
  remove () {
    var keys = document.cookie.match(/[^ =;]+(?=\=)/g);
    if(keys) {
      for(var i = keys.length; i--;)
        document.cookie = keys[i] + '=0;expires=' + new Date(0).toUTCString()
    }
    // debugger
  }
}
