/*
 * @Author: liaoxing
 * @Date: 2022-01-25 16:22:45
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-01-25 16:22:46
 * @Description: liaoxing created
 * @FilePath: \品新集成管理系统\pin-xin-admin\src\utils\localStorage.js
 */
export default {
  systemSybol: '--jstar',
  set (key, value) {
    localStorage.setItem(key + this.systemSybol, JSON.stringify(value))
  },
  get (key) {
    const val = localStorage.getItem(key + this.systemSybol)
    if (val !== undefined) {
      return JSON.parse(val)
    }
    return null
  },
  remove (key) {
    localStorage.removeItem(key + this.systemSybol)
  },
  clear () {
    const storageLength = localStorage.length
    let deleteIndex = 0
    for (let i = 0; i < storageLength; i++) {
      if (localStorage.key(deleteIndex) && localStorage.key(deleteIndex).indexOf(this.systemSybol) !== -1) {
        // this.remove(localStorage.key(deleteIndex))
        localStorage.removeItem(localStorage.key(deleteIndex))
      } else {
        deleteIndex++
      }
    }
  }
}