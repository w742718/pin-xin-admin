/*
 * @Author: liaoxing
 * @Date: 2022-01-05 14:58:34
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-03-22 09:29:22
 * @Description: 检查code格式是否正确
 * @FilePath: \pin-xin-admin\src\utils\jstar\checkCode.js
 */
const HtmlUtil = {
  /*1.用浏览器内部转换器实现html转码*/
  htmlEncode:function (html){
      let temp = document.createElement ("div");
      (temp.textContent != undefined ) ? (temp.textContent = html) : (temp.innerText = html);
      let output = temp.innerHTML;
      temp = null;
      return output;
  },
  /*2.用浏览器内部转换器实现html解码*/
  htmlDecode:function (text){
      let temp = document.createElement("div");
      temp.innerHTML = text;
      let output = temp.innerText || temp.textContent;
      temp = null;
      return output;
  }
}

export default (code, length, name) => {
  if (!code) return name + "不能为空！"
  
  if (code.toString().length > length) { // 长度限制
    return  name + '长度不能超过' + length
  }

  
  if (escape(code).indexOf( "%u" ) >= 0){ // 中文限制
    return name + '不能包含中文字符'
  }

  if (escape(code).indexOf( "%u" ) >= 0 || (code + "")[0] * 1 >= 0) {
    return name + '首字符必须为英文字母'
  }

  const regex = new RegExp("[`~!#$^&*()=|{}':;',\\[\\]<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？._]") // 特殊字符限制
  for(let i = 0 ; i < code.length ; i++){
      let a = HtmlUtil.htmlEncode(code[i])
      if(regex.test(a)){       
        return name + "不能包含特殊字符"
      }
  }
  return false
}