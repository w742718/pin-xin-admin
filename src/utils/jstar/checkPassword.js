/*
 * @Author: liaoxing
 * @Date: 2021-11-02 15:12:50
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-03-22 14:23:00
 * @Description: 检查是否包含中文和特殊字符
 * @FilePath: \pin-xin-admin\src\utils\jstar\checkPassword.js
 */
let HtmlUtil = {
  /*1.用浏览器内部转换器实现html转码*/
  htmlEncode:function (html){
      let temp = document.createElement ("div");
      (temp.textContent != undefined ) ? (temp.textContent = html) : (temp.innerText = html);
      let output = temp.innerHTML;
      temp = null;
      return output;
  },
  /*2.用浏览器内部转换器实现html解码*/
  htmlDecode:function (text){
      let temp = document.createElement("div");
      temp.innerHTML = text;
      let output = temp.innerText || temp.textContent;
      temp = null;
      return output;
  }
}

export default (value) => {
  if (value.toString().length > 11) { // 长度限制
    return '密码长度不能超过11个字符'
  }
  if (escape(value).indexOf( "%u" ) >= 0 || (value + "")[0] * 1 > 0){
    return '密码只能由英文和!开头, 并且不能包含中文';
  } else {
    const regex = new RegExp("[`~#$^&*()=|{}':;',\\[\\]<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]")
    for(let i = 0 ; i < value.length ; i++){
        let a = HtmlUtil.htmlEncode(value[i])
        if(regex.test(a)){       
          return "密码格式错误"
        }
    }
    return false
  }
}