/*
 * @Author: liaoxing
 * @Date: 2021-11-02 15:12:50
 * @LastEditors: liaoxing
 * @LastEditTime: 2021-11-02 16:25:20
 * @Description: 检查是否包含中文和特殊字符
 * @FilePath: \pin-xin-admin\src\utils\checkCode.js
 */
let HtmlUtil = {
  /*1.用浏览器内部转换器实现html转码*/
  htmlEncode:function (html){
      let temp = document.createElement ("div");
      (temp.textContent != undefined ) ? (temp.textContent = html) : (temp.innerText = html);
      let output = temp.innerHTML;
      temp = null;
      return output;
  },
  /*2.用浏览器内部转换器实现html解码*/
  htmlDecode:function (text){
      let temp = document.createElement("div");
      temp.innerHTML = text;
      let output = temp.innerText || temp.textContent;
      temp = null;
      return output;
  }
}

export default (value) => {
  if (value.length > 20) {
    return '编码最大长度为20'
  }
  if (escape(value).indexOf( "%u" ) >= 0 || (value + "")[0] * 1 > 0){
    return '编码只能由英文字母开头，且不能包含中文！';
  } else {
    const regex = new RegExp("[`~!#$^&*()=|{}':;',\\[\\]<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]")
    for(let i = 0 ; i < value.length ; i++){
        let a = HtmlUtil.htmlEncode(value[i])
        if(regex.test(a)){       
          return "编码格式错误"
        }
    }
    return false
  }
}