/*
 * @Author: liaoxing
 * @Date: 2021-10-18 18:12:33
 * @LastEditors: liaoxing
 * @LastEditTime: 2021-10-18 18:19:28
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\utils\debounce.js
 */
let debounce = (callBack, time = 200) => {
  let timeOut = null
  return () => {
    if (timeOut) {
      clearTimeout(timeOut)
    }
    timeOut = setTimeout(callBack, time)
  }
}

export default debounce