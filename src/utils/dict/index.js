/*
 * @Author: liaoxing
 * @Date: 2021-11-24 10:10:52
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-03-21 14:40:15
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\utils\dict\index.js
 */
import Dict from './Dict'
import { mergeOptions } from './DictOptions'

export default function(Vue, options) {
  mergeOptions(options)
  Vue.mixin({  
    data() {
      if (this.$options.dicts === undefined || this.$options.dicts === null) {
        return {}
      }
      const dict = new Dict()
      dict.owner = this
      return {
        dict
      }
    },
    created() {
      // console.log("触发")
      // if (!(this.dict instanceof Dict)) {
      //   return
      // }
      // options.onCreated && options.onCreated(this.dict)
      // // console.log(this.dict)
      // this.dict.init(this.$options.dicts).then(() => {
      //   options.onReady && options.onReady(this.dict)
      //   this.$nextTick(() => {
      //     this.$emit('dictReady', this.dict)
      //     if (this.$options.methods && this.$options.methods.onDictReady instanceof Function) {
      //       this.$options.methods.onDictReady.call(this, this.dict)
      //     }
      //   })
      // })
    },
  })
}
