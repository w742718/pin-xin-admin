/*
 * @Author: liaoxing
 * @Date: 2022-02-16 09:25:18
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-02-25 09:17:14
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\utils\pinxin\xiaoxinyan.js
 */
import axios from "axios"
// import sessionStorage from '@/utils/sessionStorage'
import cookies from '@/utils/cookies'
import store from '@/store'
let url = process.env.NODE_ENV == 'production' ? window.baseURL : '/'
const serve = axios.create({
  timeout: 100000,
  baseURL: window.baseURL
})
serve.interceptors.request.use(
  config => {
      let token = cookies.getCookie('userInfo')
      if (token) {
        config.headers['access_token']  = token.accessToken
      }
      return config
  },
  error => {
      Promise.reject(error)
  }
)

serve.interceptors.response.use(
  response => {
      console.log(response)
      const res = response.data
      if (response.code === 403 || response.code === 401) {
        store.dispatch('FedLogOut').then(() => {
          location.href = '/login'
        })
      }
      return res
  },
  error => {
    console.log(error)
      if (error.response.status > 400 && error.response.status < 500) {
        store.dispatch('FedLogOut').then(() => {
          location.href = '/login'
        })
      }
      return Promise.reject(error)
  }
)

export default serve