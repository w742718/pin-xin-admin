/*
 * @Author: liaoxing
 * @Date: 2022-03-02 14:22:01
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-03-04 15:34:53
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\utils\pinxin\lixiaoping.js
 */
/*
 * @Author: liaoxing
 * @Date: 2022-02-16 09:25:18
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-02-28 10:07:18
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\utils\pinxin\liquanchun.js
 */
import axios from "axios"
// import sessionStorage from '@/utils/sessionStorage'
import cookies from '@/utils/cookies'
import store from '@/store'

const serve = axios.create({
  timeout: 100000,
  baseURL: 'http://192.168.12.83:8009'
})
serve.interceptors.request.use(
  
  config => {
    console.log(3)
      let token = cookies.getCookie('userInfo')
      if (token) {
        config.headers['access_token']  = token.accessToken
      }
      return config
  },
  error => {
      Promise.reject(error)
  }
)

serve.interceptors.response.use(
  response => {
      const res = response.data
      // if (response.code === 403 || response.code === 401) {
      //   store.dispatch('FedLogOut').then(() => {
      //     location.href = '/login'
      //   })
      // }
      return res
  },
  error => {
      // if (error.response.status > 400 && error.response.status < 500) {
      //   store.dispatch('FedLogOut').then(() => {
      //     location.href = '/login'
      //   })
      // }
      return Promise.reject(error)
  }
)

export default serve