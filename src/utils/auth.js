/*
 * @Author: liaoxing
 * @Date: 2021-11-24 10:10:52
 * @LastEditors: liaoxing
 * @LastEditTime: 2021-12-15 10:38:27
 * @Description: liaoxing created
 * @FilePath: \品新集成管理系统\pin-xin-admin\src\utils\auth.js
 */
// import Cookies from 'js-cookie'
// import sessionStorage from '@/utils/sessionStorage'
import cookies from '@/utils/cookies'

const TokenKey = 'Admin-Token'

const ExpiresInKey = 'Admin-Expires-In'

export function getToken() {
  return cookies.getCookie(TokenKey)
}

export function setToken(token) {
  return cookies.setCookie(TokenKey, token)
}

export function removeToken() {
  return cookies.remove(TokenKey)
}

export function getExpiresIn() {
  return cookies.getCookie(ExpiresInKey) || -1
}

export function setExpiresIn(time) {
  return cookies.setCookie(ExpiresInKey, time)
}

export function removeExpiresIn() {
  return cookies.remove(ExpiresInKey)
}
