/*
 * @Author: liaoxing
 * @Date: 2021-11-10 16:50:56
 * @LastEditors  : maps131_liaoxing
 * @LastEditTime : 2022-03-15 16:51:04
 * @Description: 各类正则
 * @FilePath     : \pin-xin-admin\src\utils\vali.js
 */
export default {
  password: /^(?=.*\d)(?=.*[a-zA-Z])(?=.*[~!@#$%^&*])[\da-zA-Z~!@#$%^&*]{8,16}$/, // 密码
  telnumber0: /^1[34578]\d{9}$/, // 手机号码格式
  telnumber: /^1\d{10}$/, // 手机号码格式 改为不严格验证，只验证位数
  phonenumber: /^((0\d{2,3})-)(\d{7,8})$/, // 固定电话格式
  idcard: /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/, // 身份证号码
  version: /^([a-zA-Z]+)-(\d+(\.\d+){3})$/, // 版本号
  versionCode: /^\d+(\.\d+){3}$/, // 版本管理新增版本号校验
  roleName: /^[\u4E00-\u9FA5A-Za-z0-9_]+$/, // 角色名称
  account: /^(\d|\w)+$/, // 检测账号是否只由英文和数字组成
  chinese: /^[\u4E00-\u9FA5]+$/, // 只包含中文字符
  url: /^(http:\/\/\.*)|(https:\/\/\.*)/,
  intnumber: /^([1-9]\d*)$/, // 除0以外的正整数
  zeroAndnumber: /^(\d*)$/, // 0和正整数
  email: /^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/ // 邮箱
}