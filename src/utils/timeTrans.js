/*
 * @Author: liaoxing
 * @Date: 2021-12-16 14:57:02
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-03-08 16:39:23
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\utils\timeTrans.js
 */

export default {
  getYMD(time, separator = "-") {
    let date;
    if (time instanceof Date) {
      date = time;
    } else {
      date = new Date(time);
    }
    return `${pad0(date.getFullYear())}${separator}${pad0(
      date.getMonth() + 1
    )}${separator}${pad0(date.getDate())}`;
  },
  getYMDCH(time) {
    let date;
    if (time instanceof Date) {
      date = time;
    } else {
      date = new Date(time);
    }
    return `${pad0(date.getFullYear())}年${pad0(
      date.getMonth() + 1
    )}月${pad0(date.getDate())}日`;
  },
  getYMDHMS(time, separator = "-") {
    let date;
    if (time instanceof Date) {
      date = time;
    } else {
      date = new Date(time);
    }
    return `${pad0(date.getFullYear())}${separator}${pad0(
      date.getMonth() + 1
    )}${separator}${pad0(date.getDate())} ${pad0(date.getHours())}:${pad0(date.getMinutes())}:${pad0(date.getSeconds())}`;
  },
  getYM(time, separator = "-") {
    let date;
    if (time instanceof Date) {
      date = time;
    } else {
      date = new Date(time);
    }
    return `${pad0(date.getFullYear())}${separator}${pad0(
      date.getMonth() + 1
    )}`;
  },
  getYW(time, separator = "-") {
    let date;
    if (time instanceof Date) {
      date = time;
    } else {
      date = new Date(time);
    }
    return `${pad0(date.getFullYear())}${separator}${pad0(getWeek(date))}`;
  },
};

function getWeek(dt) {
  let d1 = new Date(dt);
  let d2 = new Date(dt);
  d2.setMonth(0);
  d2.setDate(1);
  let rq = d1 - d2;
  let days = Math.ceil(rq / (24 * 60 * 60 * 1000));
  let num = Math.ceil(days / 7);
  return num;
}

function pad0(str = "", count = 2) {
  let padStr = "";
  str = str + "";
  let add = count - str.length;
  if (add > 0) {
    for (let i = 0; i < add; ++i) {
      padStr += "0";
    }
  }
  return padStr + str;
}
