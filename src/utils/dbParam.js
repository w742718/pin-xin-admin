import axios from 'axios'
// import sessionStorage from '@/utils/sessionStorage'
import cookies from '@/utils/cookies'
import store from '@/store'
// let url = process.env.NODE_ENV == 'production' ? window.baseURL : '/'
const serves = axios.create({
  timeout: 1000000,
  baseURL: window.baseURL + "/report/jmreport/"
})
serves.interceptors.request.use(
  config => {
      let token = cookies.getCookie('userInfo')
      if (token) {
        config.headers['access_token']  = token.accessToken
      }
      return config
  },
  error => {
      Promise.reject(error)
  }
)

serves.interceptors.response.use(
  response => {
      const res = response.data
      if (response.code === 403 || response.code === 401) {
        // store.dispatch('FedLogOut').then(() => {
        //   location.href = '/login'
        // })
      }
      
      if (response.config.responseType == "arraybuffer") {
        return response
      } else {
        return res
      }
  },
  error => {
    console.log(error)
      if (error.response.status > 400 && error.response.status < 500) {
        // store.dispatch('FedLogOut').then(() => {
        //   location.href = '/login'
        // })
      }
      return Promise.reject(error)
  }
)

export default serves