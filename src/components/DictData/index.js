/*
 * @Author: liaoxing
 * @Date: 2021-11-24 10:10:51
 * @LastEditors: liaoxing
 * @LastEditTime: 2021-12-02 14:20:24
 * @Description: liaoxing created
 * @FilePath: \品新集成管理系统\pin-xin-admin\src\components\DictData\index.js
 */
import Vue from 'vue'
import DataDict from '@/utils/dict'
import { getDicts as getDicts } from '@/api/system/dict/data'

function install() {
  Vue.use(DataDict, {
    metas: {
      '*': {
        labelField: 'dictLabel',
        valueField: 'dictValue',
        request(dictMeta) {
          return getDicts(dictMeta.type).then(res => res.data)
        },
      },
    },
  })
}

export default {
  install,
}