/*
 * @Author: liaoxing
 * @Date: 2021-10-14 17:36:29
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-03-21 13:54:58
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\permission.js
 */
import router from './router'
import store from './store'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import { getToken } from '@/utils/auth'

NProgress.configure({ showSpinner: false })
const whiteList = ['/login', '/auth-redirect', '/bind', '/register']

router.beforeEach((to, from, next) => {
  console.log(to)
  // return
  // if (to.path != '/process') return
  // debugger
  NProgress.start()
  
  if (getToken()) {
    // to.meta.menuName && store.dispatch('settings/setTitle', to.meta.menuName)
    /* has token*/
    if (to.path === '/login') {
      next({ path: '/' })
      NProgress.done()
    } else {
      
      if (store.getters.roles.length === 0) {
        // 判断当前用户是否已拉取完user_info信息
        store.dispatch('GetInfo').then(() => {
          
          store.dispatch('GenerateRoutes').then(accessRoutes => {
            
            // 根据roles权限生成可访问的路由表
            // router.matcher = new Router().matcher
            router.addRoutes(accessRoutes) // 动态添加可访问路由表
            next({ ...to, replace: true }) // hack方法 确保addRoutes已完成
          })
        }).catch(err => {
          console.log(err)
            // store.dispatch('LogOut').then(() => {
            //   Message.error(err)
            //   next({ path: '/' })
            // })
        })
        
      } else {
        next()      
      }
    }
  } else {
    // 没有token
    if (whiteList.indexOf(to.path) !== -1) {
      // 在免登录白名单，直接进入
      next()
    } else {
      if (window.location.search.indexOf('code') > -1) {
        next()
      } else {
        next(`/login`) // 否则全部重定向到登录页
      }
      // next()
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  NProgress.done()
})

