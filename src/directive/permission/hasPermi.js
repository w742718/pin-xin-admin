/*
 * @Author: liaoxing
 * @Date: 2021-11-24 10:10:52
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-03-21 14:43:49
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\src\directive\permission\hasPermi.js
 */
 /**
 * v-hasPermi 操作权限处理
 * Copyright (c) 2019 ruoyi
 */
 
import store from '@/store'

export default {
  inserted(el, binding, vnode) {
    const { value } = binding
    const all_permission = "*:*:*";
    const permissions = store.getters && store.getters.permissions
    console.log('**', permissions)
    if (value && value instanceof Array && value.length > 0) {
      const permissionFlag = value

      const hasPermissions = permissions.some(permission => {
        return all_permission === permission || permissionFlag.includes(permission)
      })

      if (!hasPermissions) {
        el.parentNode && el.parentNode.removeChild(el)
      }
    } else {
      throw new Error(`请设置操作权限标签值`)
    }
  }
}
