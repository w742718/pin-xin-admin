/*
 * @Author: liaoxing
 * @Date: 2021-12-15 10:46:53
 * @LastEditors: liaoxing
 * @LastEditTime: 2022-03-04 14:14:12
 * @Description: liaoxing created
 * @FilePath: \pin-xin-admin\public\domain.js
 */
window.baseURL = 'http://192.168.8.140:8100' // 请求域名
window.repoortURL = "http://192.168.8.140:8100/report" // 报表域名
// window.dbURL = 'http://192.168.12.83:8009' // 请求域名
// window.dbURL = 'http://192.168.8.140:8100'
window.dbURL = 'http://192.168.8.140:8100'
window.webUrl = window.location.origin + '/index' // 扫码跳转地址

window.tempURL = "http://192.168.12.150:8009"

window.projectOptions = [
  { label: "基本电费", value: "成本,固定费用,基本电费" },
  { label: "设备折旧", value: "成本,固定费用,设备折旧" },
  { label: "装修费", value: "成本,固定费用,装修费" },
  { label: "宿舍租金", value: "成本,固定费用,宿舍租金" },
  { label: "物业管理费", value: "成本,固定费用,物业管理费" },
  { label: "厂租", value: "成本,固定费用,厂租" },
  { label: "食堂租金", value: "成本,固定费用,食堂租金" },
  { label: "资金成本", value: "成本,资金成本" },
  { label: "厂房水电费", value: "成本,变动费用,厂房水电费" },
  { label: "社保", value: "成本,变动费用,社保" },
  { label: "管理人员工资", value: "成本,工资,管理人员工资" },
  { label: "运输费", value: "成本,运营费用,运输费" },
  { label: "办公费", value: "成本,经营费用,办公费" },
  { label: "电信费", value: "成本,经营费用,电信费" },
  { label: "快递费", value: "成本,经营费用,快递费" },
  { label: "汽车费用", value: "成本,经营费用,汽车费用" },
]